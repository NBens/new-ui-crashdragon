const app = Vue.createApp({})

app.component('danger-panel', {
  props: ['title', 'number'],
  template: `<div class="stati bg-alizarin">
  <i class="icon-exclamation icons"></i>
  <div>
    <b>{{ number }}</b>
    <span>{{ title }}</span>
  </div> 
</div>`
})

app.component('warning-panel', {
  props: ['title', 'number'],
  template: `<div class="stati bg-carrot">
  <i class="icon-docs icons"></i>
  <div>
    <b>{{ number }}</b>
    <span>{{ title }}</span>
  </div> 
</div>`
})

app.component('green-panel', {
  props: ['title', 'number'],
  template: `<div class="stati bg-emerald">
  <i class="icon-check icons"></i>
  <div>
    <b>{{ number }}</b>
    <span>{{ title }}</span>
  </div> 
</div>`
})

app.mount('#mount')

new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: [00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
    datasets: [{ 
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "2.03 Alpha",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "2025 Beta",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "60.2 Alpha",
        borderColor: "#3cba9f",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Reports of Product 1 All Versions today'
    }
  }
});

new Chart(document.getElementById("pie-chart"), {
  type: 'pie',
  data: {
    labels: ["Reports", "Crashes", "Solved Crashes"],
    datasets: [{
      label: "Number",
      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
      data: [150,300,500]
    }]
  },
  options: {
    title: {
      display: true,
      text: 'Breakdown of Reports & Crashes for Product 1 version 2 Alpha'
    }
  }
});

const dataTable = new simpleDatatables.DataTable("#usersTable", {
	searchable: true
})